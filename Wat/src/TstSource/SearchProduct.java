package TstSource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import ActTst.insertSearch;

public class SearchProduct {

	private WebDriver driver;

	private insertSearch insertSearch;

	@Before
	public void SetUpTest() {
		driver = new FirefoxDriver();

		insertSearch = new insertSearch();
		driver.get("http://www.williams-sonoma.com");
		insertSearch = PageFactory
				.initElements(this.driver, insertSearch.class);

	}

	String productNameSearch;
	String productNameQuick;
	String productPriceSearch;
	String productPriceQuick;

	@Test
	public void Add() {

		insertSearch.searchProduct();
		insertSearch.searchBtn();
		productNameSearch = insertSearch.productName();
		insertSearch.quickLookButton();
		productNameQuick = insertSearch.productNameQuickLook();
		productPriceQuick = insertSearch.priceOverlay();
		productPriceQuick = insertSearch.priceResultPage();

		Assert.assertEquals(productNameSearch, productNameQuick);
		Assert.assertEquals("priceOverlay", "priceResultPage");
	}

}
