package TstSource;


import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import ActTst.AddCart;

public class TstAdd {

	private WebDriver driver;
	private AddCart addcart;

	@Before
	public void SetUpTest() {
		driver = new FirefoxDriver();
		addcart = new AddCart(driver);

		driver.get("http://www.williams-sonoma.com");
		addcart = PageFactory.initElements(this.driver, AddCart.class);

	}

	@Test
	public void Add() {

		addcart.navigatehoverCookWareSets();
		addcart.selectedProduct();
		addcart.addCartButton();
		addcart.expressCheckOutButton();

	}

}
