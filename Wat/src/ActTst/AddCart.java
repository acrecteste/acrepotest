package ActTst;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddCart {

	@FindBy(how = How.XPATH, using = "/html/body/header/nav/ul/li[1]/a")
	private static WebElement hoverCookWare;

	@FindBy(how = How.XPATH, using = "/html/body/header/nav/ul/li[1]/div/div[1]/ul[1]/li[1]/a")
	private static WebElement hoverCookWareSets;
	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div[2]/div[3]/ul/li[1]/div[1]/a[1]/span/img")
	private static WebElement selectedProduct;

	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div[1]/div[7]/div[2]/div[2]/section/div/div/fieldset[1]/button")
	private static WebElement addCartButton;

	@FindBy(how = How.ID, using = "anchor-btn-checkout")
	private static WebElement expressCheckOutButton;

	

	private static WebDriver webDriver;

	public AddCart(WebDriver webDriver) {
		this.webDriver = webDriver;

	}

	public static void navigatehoverCookWareSets() {
		waitingObject(hoverCookWare);
		Actions builder = new Actions(webDriver);
		Actions hoverOverRegistrar = builder.moveToElement(hoverCookWare);
		hoverOverRegistrar.perform();
		hoverCookWareSets.click();
	}

	public static void waitingObject(WebElement object) {
		boolean regex = object.isEnabled();
		while (!regex) {
			WebDriverWait wait = new WebDriverWait(webDriver, 1000);
			WebElement element = wait.until(ExpectedConditions
					.elementToBeClickable(object));
			regex = element.isEnabled();
		}
	}

	public void selectedProduct() {

		waitingObject(selectedProduct);
		selectedProduct.click();

	}

	public static void waitingObjectClickProduct(WebElement object) {
		boolean regex = object.isEnabled();
		while (!regex) {
			WebDriverWait wait = new WebDriverWait(webDriver, 1000);
			WebElement element = wait.until(ExpectedConditions
					.elementToBeClickable(object));
			regex = element.isEnabled();

		}
	}

	public void addCartButton() {

		waitingObject(addCartButton);
		addCartButton.click();
	}

	public static void waitingObjectClick(WebElement object) {
		boolean regex = object.isEnabled();
		while (!regex) {
			WebDriverWait wait = new WebDriverWait(webDriver, 1000);
			WebElement element = wait.until(ExpectedConditions
					.elementToBeClickable(object));
			regex = element.isEnabled();

		}
	}

	public void expressCheckOutButton() {

		waitingObjectClick(expressCheckOutButton);
		expressCheckOutButton.click();
	}

	public static void waitingObjectCheckOutClick(WebElement object) {
		boolean regex = object.isEnabled();
		while (!regex) {
			WebDriverWait wait = new WebDriverWait(webDriver, 3000);
			WebElement element = wait.until(ExpectedConditions
					.elementToBeClickable(object));
			regex = element.isEnabled();

		}
	}



	

	
}
