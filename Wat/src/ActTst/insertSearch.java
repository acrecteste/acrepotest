package ActTst;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class insertSearch {

	@FindBy(how = How.ID, using = "search-field")
	private static WebElement searchProduct;

	@FindBy(how = How.ID, using = "btnSearch")
	private static WebElement searchBtn;

	@FindBy(how = How.XPATH, using = "pip-/html/body/div[1]/div/div[2]/div[2]/div[2]/ul/li[7]/a/span")
	private static WebElement productName;

	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div[2]/div[2]/div[2]/ul/li[7]/div[1]/a[2]/span")
	private static WebElement quickLookButton;

	@FindBy(how = How.CLASS_NAME, using = "product-thumb")
	private static WebElement productNameQuickLook;

	@FindBy(how = How.XPATH, using = "/html/body/div[4]/div/div/div/div[2]/div/div[4]/ul[2]/li[1]/div/section/section/div/div/div/div[2]/span/span[2]/span[2]/span[2]")
	private static WebElement priceOverlay;

	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div[2]/div[2]/div[2]/ul/li[7]/span/span[2]/span[2]/span[2]")
	private static WebElement priceResultPage;
	private static WebDriver webDriver;

	public void searchProduct() {

		searchProduct.sendKeys("fry pan");
		searchProduct.getText();
	}

	public String productName() {
		waitingObjectCheck(productName);

		return productName
				.getCssValue("All-Clad d5 Brushed Stainless-Steel Nonstick Fry Pan");

	}

	public static void waitingObjectCheck(WebElement object) {
		boolean regex = object.isEnabled();
		while (!regex) {
			WebDriverWait wait = new WebDriverWait(webDriver, 3000);
			WebElement element = wait.until(ExpectedConditions
					.elementToBeClickable(object));
			regex = element.isEnabled();
		}
	}

	public void searchBtn() {

		searchBtn.click();

	}

	public void quickLookButton() {
		quickLookButton.click();

	}

	public String productNameQuickLook() {
		waitingObjectCheck(productNameQuickLook);
		return productNameQuickLook
				.getCssValue("All-Clad d5 Brushed Stainless-Steel Nonstick Fry Pans");

	}

	public static void waitingObjectCheckOnOverlay(WebElement object) {
		boolean regex = object.isEnabled();
		while (!regex) {
			WebDriverWait wait = new WebDriverWait(webDriver, 3000);
			WebElement element = wait.until(ExpectedConditions
					.elementToBeClickable(object));
			regex = element.isEnabled();
		}
	}

	public String priceOverlay() {
		return priceOverlay.getCssValue("99.95");

	}

	public String priceResultPage() {
		return priceResultPage.getCssValue("99.95");

	}
}
